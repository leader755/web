### 语雀文章用Serverless自动部署到Hexo博客


#### Serverless配置

```python
# -*- coding: utf8 -*-
import requests

def main_handler(event, context):
    r = requests.post("https://api.github.com/repos/用户名/私有仓库名/dispatches",
    json = {"event_type": "start"},
    headers = {"User-Agent":'curl/7.52.1',
              'Content-Type': 'application/json',
              'Accept': 'application/vnd.github.everest-preview+json',
              'Authorization': 'token Github访问Token'})

    if r.status_code == 204:
        return "This's OK!"
    else:
        return r.status_code
```

#### 语雀配置 package.json

```yaml
"yuqueConfig": {
    "baseUrl": "https://www.yuque.com/api/v2",
    "login": "你的语雀名，我这里的是ccne2020",
    "repo": "你的语雀知识库名，我这里的是hexoblog",
    "mdNameFormat": "title",
    "postPath": "source/_posts/yuque",
    "onlyPublished": false
  },
```


#### 配置Github Actions脚本


```yaml
# workflow name
name: Yuque To Github Pages

# 当有 push 到仓库和外部触发的时候就运行
on: [push, repository_dispatch]

# YQ_TOKEN
# YUQUE_GIT_HEXO
jobs:
  deploy:
    name: Deploy Hexo Public To Pages
    runs-on: ubuntu-latest
    env:
      TZ: Asia/Shanghai

    steps:
    # check it to your workflow can access it
    # from: https://github.com/actions/checkout
    - name: Checkout Repository master branch
      uses: actions/checkout@master

    # from: https://github.com/actions/setup-node
    - name: Setup Node.js 10.x
      uses: actions/setup-node@master
      with:
        node-version: "10.x"

    # from https://github.com/x-cold/yuque-hexo
    - name: Setup Hexo Dependencies and Generate Public Files
      env:
    # from: 这里的YUQUE_TOKEN: $不能随意改
        YUQUE_TOKEN: ${{ secrets.YQ_TOKEN }}
      run: |
        npm install hexo-cli -g
        npm install yuque-hexo -g
        npm install
        npm run start

    # from https://github.com/peaceiris/actions-gh-pages
    - name: Deploy
      uses: peaceiris/actions-gh-pages@v3
      with:
        deploy_key: ${{ secrets.YUQUE_GIT_HEXO }}
        external_repository: Github用户名/Pages仓库
        publish_branch: master
        publish_dir: ./public
        commit_message: ${{ github.event.head_commit.message }}
```
