
### 配置coding持续集成-方案1

```
pipeline {
agent any
stages {
    stage('检出') {
      steps {
        checkout([$class: 'GitSCM', branches: [[name: env.GIT_BUILD_REF]],
                                            userRemoteConfigs: [[url: env.GIT_REPO_URL, credentialsId: env.CREDENTIALS_ID]]])
      }
    }
    stage('构建') {
      steps {
        echo '构建中...'
        sh 'node -v'
        sh 'npm install -g hexo-cli'
        sh 'npm install hexo --save'
        sh 'npm install -g hexo-generator-searchdb'
        sh 'npm install -g'
        echo '构建完成.'
      }
    }
    stage('测试') {
      steps {
        echo '单元测试中...'
        sh 'hexo clean'
        sh 'hexo g '
        echo '单元测试完成.'
      }
    }
    stage('部署') {
      steps {
        echo '部署中...'
        sh 'npm install hexo-deployer-git --save'
        sh 'hexo deploy'
        echo '部署完成'
      }
    }
  }
}
```


### 配置coding持续集成-方案2


 ```
pipeline {
     agent {
        docker {
            registryUrl 'https://coding-public-docker.pkg.coding.net'
            image 'public/docker/nodejs:12'
        }
    }
  stages {
    stage('检出') {
      steps {
        checkout([$class: 'GitSCM', branches: [[name: env.GIT_BUILD_REF]], userRemoteConfigs: [[url: env.GIT_REPO_URL, credentialsId: env.CREDENTIALS_ID]]])
      }
    }
    stage('环境') {
      steps {
        echo '构建中...'
        sh 'npm config set registry http://mirrors.cloud.tencent.com/npm/'
        sh 'npm install'
        sh 'node -v && npm -v'
        sh 'npm install -g hexo-cli'
        sh 'npm install hexo --save'
        sh 'npm install -g hexo-generator-searchdb'
        sh 'npm install -g'
        sh 'hexo -v'
        echo '构建完成.'
      }
    }
    stage('生产') {
      steps {
        echo '生产中...'
        sh 'hexo clean'
        sh 'hexo g'
        echo '生产完成.'
      }
    }
    stage('部署') {
      steps {
        echo '部署中...'
        dir(path: 'public') {
          sh 'ls'
          sh 'git init'
          sh 'git config user.name $USER_NAME'
          sh 'git config user.email $USER_EMAIL'
          sh 'git add -A'
          sh 'git commit -m "$GIT_COMMIT"'
          sh 'git push -u -f "$USER_PROJECT" master:master'
        }
        echo '部署完成'
      }
    }
  }
}
```