### coding持续集成同步到GitHub


```
pipeline {
  agent any
  stages {
    stage('检出') {
      steps {
        checkout([
          $class: 'GitSCM',
          branches: [[name: env.GIT_BUILD_REF]],
          userRemoteConfigs: [[url: env.GIT_REPO_URL, credentialsId: env.CREDENTIALS_ID]]
        ])
      }
    }
    stage('推送部署') {
      steps {
        echo '正在推送文件...'
        sh 'git fetch $FETCH'
        sh 'git push -f $FETCH HEAD:master'
        echo '已完成推送.'
      }
    }
  }
}
```