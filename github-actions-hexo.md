### Github Actions自动部署Hexo博客

  ```
# workflow name
name: Hexo Github Actions

# master branch on push, auto run
on:
  push:
    branches:
      - master

jobs:
  build:
    runs-on: ubuntu-latest

    steps:
      # check it to your workflow can access it
      # from: https://github.com/actions/checkout
      - name: Checkout Repository master branch
        uses: actions/checkout@master

      # from: https://github.com/actions/setup-node
      - name: Setup Node.js 10.x
        uses: actions/setup-node@master
        with:
          node-version: "10.x"

      - name: Setup Hexo Dependencies
        run: |
          npm install hexo-cli -g
          npm install

      - name: Setup Deploy Private Key
        env:
          HEXO_DEPLOY_PRIVATE_KEY: ${{ secrets.HEXO_DEPLOY_PRIVATE_KEY }}
        run: |
          sudo timedatectl set-timezone "Asia/Shanghai"
          mkdir -p ~/.ssh/
          echo "$HEXO_DEPLOY_PRIVATE_KEY" > ~/.ssh/id_rsa 
          chmod 600 ~/.ssh/id_rsa
          ssh-keyscan github.com >> ~/.ssh/known_hosts

      - name: Setup Git Infomation
        run: |
          git config --global user.name 'Github用户名' 
          git config --global user.email 'Github注册邮箱'
      - name: Deploy Hexo
        run: |
          hexo clean
          hexo generate 
          hexo deploy
```