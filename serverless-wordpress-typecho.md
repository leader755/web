# Serverless云函数计算搭建Typecho和Wordpress网站

因博客复制代码会打乱原有结构，故此放到此处。

## 在WordPress主题`functions.php`追加如下代码：

```
//http转https
add_filter('get_header', 'fanly_ssl');
function fanly_ssl(){
	if( is_ssl() ){
		function fanly_ssl_main ($content){
			$siteurl = get_option('siteurl');
			$upload_dir = wp_upload_dir();
			$content = str_replace( 'http:'.strstr($siteurl, '//'), 'https:'.strstr($siteurl, '//'), $content);
			$content = str_replace( 'http:'.strstr($upload_dir['baseurl'], '//'), 'https:'.strstr($upload_dir['baseurl'], '//'), $content);
			return $content;
		}
		ob_start("fanly_ssl_main");
	}
}
```

## 在wp-config.php追加如下代码
```
$_SERVER['HTTPS'] = 'on';
define('FORCE_SSL_LOGIN', true);
define('FORCE_SSL_ADMIN', true);
```